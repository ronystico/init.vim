call plug#begin('~/.config/nvim/Plugins')

Plug 'morhetz/gruvbox'
Plug 'itchyny/lightline.vim'
Plug 'mattn/emmet-vim'
Plug 'preservim/nerdtree'
Plug 'mbbill/undotree'
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'shinchu/lightline-gruvbox.vim'
" you need bloat for rich presence, added more bloat (autocomplete frontend)
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neoclide/coc-html', {'do': 'yarnpkg install --frozen-lockfile'}
Plug 'neoclide/coc-css', {'do': 'yarnpkg install --frozen-lockfile'}
Plug 'neoclide/coc-tsserver', {'do': 'yarnpkg install --frozen-lockfile'}
" Show css colors inside vim
Plug 'ap/vim-css-color'

call plug#end()

" Select coc suggest with <CR> key (enter) and format code
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" variable to make the bloat install botnet rpc
let g:coc_global_extensions = ['coc-discord-rpc']

"LightLine config
let g:lightline = {}
let g:lightline.colorscheme = 'gruvbox'

set title  " Muestra el nombre del archivo en la ventana de la terminal
set number  " Muestra los números de las líneas
set mouse=a  " Permite la integración del mouse (seleccionar texto, mover el cursor)

set nowrap  " No dividir la línea si es muy larga

set cursorline  " Resalta la línea actual
set colorcolumn=120  " Muestra la columna límite a 120 caracteres

" Indentación a 2 espacios (4 spaces is better 8-))
set tabstop=2
set shiftwidth=2
set softtabstop=2
set shiftround
set expandtab  " Insertar espacios en lugar de <Tab>s

set hidden  " Permitir cambiar de buffers sin tener que guardarlos

set ignorecase  " Ignorar mayúsculas al hacer una búsqueda
set smartcase  " No ignorar mayúsculas si la palabra a buscar contiene mayúsculas

set spelllang=en,es  " Corregir palabras usando diccionarios en inglés y español

nnoremap <C-n> :NERDTreeToggle<CR>

set termguicolors  " Activa true colors en la terminal
set background=dark  " Fondo del tema: light o dark
colorscheme gruvbox  " Nombre del tema
